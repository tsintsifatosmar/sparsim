# SPARSim

SPARSim is an R tool for the simulation of single cell RNA-seq (scRNA-seq) count table.

To cite this package please use  ***"SPARSim single cell: a count data simulator for scRNA-seq data", Giacomo Baruzzo, Ilaria Patuzzi, Barbara Di Camillo, Bioinformatics, Volume 36, Issue 5, March 2020, Pages 1468–1475,*** https://doi.org/10.1093/bioinformatics/btz752


## Installation

#### Install from GitLab

SPARSim is available on GitLab at https://gitlab.com/sysbiobig/sparsim

To install SPARSim from GitLab, please use the following commands:
```r
library(devtools)
install_gitlab("sysbiobig/sparsim", build_opts = c("--no-resave-data", "--no-manual"), build_vignettes = TRUE)
```
The above commands would install SPARSim, the required dependencies and SPARSim vignette. 

To install SPARSim without its vignette, please use the following commands:
```r
library(devtools)
install_gitlab("sysbiobig/sparsim")
```



#### Install from source package

SPARSim R package can be downloaded at http://sysbiobig.dei.unipd.it/?q=SPARSim

It requires packages *RCpp*, *Matrix*, *scran* and *edgeR* to work.

To install from source, please use the following command:
```r
install.packages("SPARSim_0.9.5.tar.gz", repos = NULL, type = "source")
```

## Getting started

Please browse SPARSim vignettes using the following commands:

```r
library(SPARSim)
browseVignettes("SPARSim")
```

